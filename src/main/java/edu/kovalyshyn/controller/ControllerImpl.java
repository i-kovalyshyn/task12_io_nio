package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.Model;
import edu.kovalyshyn.model.ModelImpl;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }


    @Override
    public void testSerialize() {
        model.testSerialize();
    }

    @Override
    public void performance() {
        model.performance();
    }

    @Override
    public void contentDirectory() {
        model.contentDirectory();
    }

    @Override
    public void nIo() {
        model.nIo();
    }
}
