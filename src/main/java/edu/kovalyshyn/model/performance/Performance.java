package edu.kovalyshyn.model.performance;

import edu.kovalyshyn.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.UUID;

public class Performance {
    private final static Logger log = LogManager.getLogger(MyView.class);
    private static final String pathTo156Mb = "/media/ihor/Work/JAVA/BOOKS/Шилдт Г. - Java. Полное руководство, 10-ое издание - 2018.pdf";
    private static final String pathTo750Kb = "/media/ihor/Work/JAVA/BOOKS/JPA_Mini_Book.pdf";
        private static final int bufferSize = 1024 * 512;

    public void ReadingAndWritingPerformance() {
        byte[] fileAllBytes = bufferedReadFileAllBytes();
        bufferedWriteFile(fileAllBytes);
    }

    private static void bufferedWriteFile(byte[] fileAllBytes) {
        String pathToWriteLargeFile = pathTo156Mb + UUID.randomUUID() ;
        log.info("bufferedWriteFile");
        long startTime = System.nanoTime();
        try (BufferedOutputStream bufferedOutputStream =
                     new BufferedOutputStream(
                             new FileOutputStream(pathToWriteLargeFile), bufferSize)) {
            bufferedOutputStream.write(fileAllBytes);
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        printTimeExecuting(startTime);
    }

    private static byte[] bufferedReadFileAllBytes() {
        log.info("bufferedReadFile");
        long startTime = System.nanoTime();
        try (BufferedInputStream bufferedInputStream =
                     new BufferedInputStream(new FileInputStream(pathTo156Mb), bufferSize)) {
            byte[] bytes = bufferedInputStream.readAllBytes();
            printTimeExecuting(startTime);
            return bytes;
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return new byte[0];
    }

    public static void bufferedReadFile() {
        log.info("bufferedReadFile");
        long startTime = System.nanoTime();
        int count = 0;
        try (BufferedInputStream bufferedInputStream =
                     new BufferedInputStream(new FileInputStream(pathTo156Mb), bufferSize)) {
            int data = bufferedInputStream.read();
            while (data != -1) {
                data = bufferedInputStream.read();
                count++;
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("Bytes = " + count);

        printTimeExecuting(startTime);
    }

    public static void usualReadFile() {
        log.info("usualReadFile");
        long startTime = System.nanoTime();
        int count = 0;
        try (InputStream fileInputStream = new FileInputStream(pathTo156Mb)) {
            int data = fileInputStream.read();
            while (data != -1) {
                data = fileInputStream.read();
                count++;
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("Bytes = " + count);

        printTimeExecuting(startTime);
    }

    private static void printTimeExecuting(long startTime) {
        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        log.info("Total time  - " + (double) totalTime / 1_000_000_000 + "sec.");
    }
}
