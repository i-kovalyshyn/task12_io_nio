package edu.kovalyshyn.model.content_directory;

import edu.kovalyshyn.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;

public class ContentDirectory {
    private final static Logger log = LogManager.getLogger(MyView.class);
    private static final String path = "/home/ihor/Calibre Library";

    public  void contentDirectory() {
        File file = new File(path);
        if (file.exists()) {
            printFile(file, "");
        } else {
            log.info("not exist");
        }
    }

    private static void printFile(File file, String s) {
        File[] files = file.listFiles();
        s += " ";
        for (File f : files) {
            if (f.isDirectory()) {
                log.info(s + f.getPath());
                printFile(f, s);
            } else {
                printFileInfo(s, f);
            }
        }
    }

    private static void printFileInfo(String s, File f) {
        log.info(s + f.getName()
                + ", can exec - " + f.canExecute()
                + ", can read - " + f.canRead()
                + ", can write - " + f.canWrite()
                + ", is hidden - " + f.isHidden()
                + ", last modified - " + new Date(f.lastModified())
        );
    }
}
