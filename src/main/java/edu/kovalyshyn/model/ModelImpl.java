package edu.kovalyshyn.model;

import edu.kovalyshyn.model.content_directory.ContentDirectory;
import edu.kovalyshyn.model.nio.NiO;
import edu.kovalyshyn.model.performance.Performance;
import edu.kovalyshyn.model.serialization.Ship;
import edu.kovalyshyn.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ModelImpl implements Model {
    private static Logger log = LogManager.getLogger(MyView.class);

    @Override
    public void testSerialize() {
        Ship ship = new Ship("Ship");
        log.info(ship);

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream("myShip.dat"));
            objectOutputStream.writeObject(ship);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ship.staticValue = 50;

        try {
            ObjectInputStream inputStream = new ObjectInputStream(
                    new FileInputStream("myShip.dat"));
            Ship myShipFile = (Ship) inputStream.readObject();
            inputStream.close();
            log.info(myShipFile);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void performance() {
        new Performance().ReadingAndWritingPerformance();
    }

    @Override
    public void contentDirectory() {
        new ContentDirectory().contentDirectory();
    }

    @Override
    public void nIo() {
        new NiO().nIo();
    }
}

