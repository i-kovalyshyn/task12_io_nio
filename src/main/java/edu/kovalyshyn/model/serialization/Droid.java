package edu.kovalyshyn.model.serialization;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Droid implements Serializable {

    private int id;
    private String name;
    private Double version;
    private transient int droidTransient;



}
