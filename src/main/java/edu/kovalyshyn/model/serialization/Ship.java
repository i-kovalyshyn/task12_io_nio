package edu.kovalyshyn.model.serialization;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Data
public class Ship implements Serializable {
    public static int staticValue = 10;
    private String name;
    private List<Droid> droids;


    public Ship(String name) {
        this.name = name;
        this.droids = fillDroids();
    }

    private List<Droid> fillDroids() {
        return new ArrayList<>(Arrays.asList(
                new Droid(1, "Droid-1", 1.01, 100),
                new Droid(2, "Droid-2", 2.02, 102),
                new Droid(3, "Droid-3", 3.05, 103)
        ));
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", droids=" + droids + ", staticValue="+staticValue+
                '}';
    }
}
